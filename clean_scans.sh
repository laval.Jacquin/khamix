 # clean results:
echo
rm out*
rm -r estimates_h0
rm -r genome_scan_chromo_num*
rm -r results_genome_scan
rm data_parameters/kernel_index.txt
rm data_parameters/nb_snp_hap.txt
rm data_parameters/nb_chromosomes.txt
rm data_parameters/trait_name.txt
rm data_parameters/signif_level.txt
